(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Tenant.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Tenant.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_tenant_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/tenant_service */ "./resources/js/services/tenant_service.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Tenant",
  data: function data() {
    return {
      tenants: {},
      tenantData: [],
      checkedTenantId: [],
      update: null,
      buildings: [],
      search: null,
      editTenantData: {},
      error: null,
      editError: null,
      filterSelect: null,
      direction: 'asc',
      sort: null,
      page: 1
    };
  },
  mounted: function mounted() {
    this.getTenants();
    this.getBuildings();
  },
  watch: {
    search: function search(val, old) {
      if (val === "") {
        this.getTenants();
      } else {
        this.searchData();
      }
    }
  },
  methods: {
    hideNewTenantModel: function hideNewTenantModel() {
      this.$refs.newTenantModel.hide();
    },
    showNewTenantModel: function showNewTenantModel() {
      this.$refs.newTenantModel.show();
    },
    sortChange: function sortChange(key) {
      this.direction = this.sort !== key || this.direction === 'desc' ? 'asc' : 'desc';
      this.sort = key;
      this.getTenants(1);
    },
    filterby: function () {
      var _filterby = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.getTenants(1);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function filterby() {
        return _filterby.apply(this, arguments);
      }

      return filterby;
    }(),
    searchData: function () {
      var _searchData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this.getTenants(1);

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function searchData() {
        return _searchData.apply(this, arguments);
      }

      return searchData;
    }(),
    getTenants: function () {
      var _getTenants = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var page,
            params,
            response,
            _args3 = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                page = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : 1;
                params = {
                  page: page
                };

                if (this.sort !== null) {
                  params['sort'] = this.sort;
                  params['direction'] = this.direction;
                }

                if (this.search !== null) params['search'] = this.search;
                if (this.filterSelect !== null) params['filter'] = this.filterSelect;
                _context3.prev = 5;
                _context3.next = 8;
                return _services_tenant_service__WEBPACK_IMPORTED_MODULE_1__["getTenant"](params);

              case 8:
                response = _context3.sent;
                this.tenants = response.data;
                _context3.next = 15;
                break;

              case 12:
                _context3.prev = 12;
                _context3.t0 = _context3["catch"](5);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 15:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[5, 12]]);
      }));

      function getTenants() {
        return _getTenants.apply(this, arguments);
      }

      return getTenants;
    }(),
    getBuildings: function () {
      var _getBuildings = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _services_tenant_service__WEBPACK_IMPORTED_MODULE_1__["getBuilding"]();

              case 3:
                response = _context4.sent;
                this.buildings = response.data.data;
                _context4.next = 10;
                break;

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 7]]);
      }));

      function getBuildings() {
        return _getBuildings.apply(this, arguments);
      }

      return getBuildings;
    }(),
    createTenant: function () {
      var _createTenant = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (this.tenantData.name_en) {
                  _context5.next = 4;
                  break;
                }

                this.error = "Name EN required.";
                _context5.next = 38;
                break;

              case 4:
                if (this.tenantData.name_ar) {
                  _context5.next = 8;
                  break;
                }

                this.error = 'Name AR required.';
                _context5.next = 38;
                break;

              case 8:
                if (this.tenantData.building_id) {
                  _context5.next = 12;
                  break;
                }

                this.error = 'Building required.';
                _context5.next = 38;
                break;

              case 12:
                this.error = null;
                formData = new FormData();
                formData.append('name_en', this.tenantData.name_en);
                formData.append('name_ar', this.tenantData.name_ar);
                formData.append('active', 1);
                formData.append('building_id', this.tenantData.building_id);
                _context5.prev = 18;
                _context5.next = 21;
                return _services_tenant_service__WEBPACK_IMPORTED_MODULE_1__["createTenant"](formData);

              case 21:
                response = _context5.sent;
                this.errors = {};
                this.tenants.data.unshift(response.data.data);
                this.hideNewTenantModel();
                this.flashMessage.success({
                  title: 'Tenant Successfully Created'
                });
                _context5.next = 38;
                break;

              case 28:
                _context5.prev = 28;
                _context5.t0 = _context5["catch"](18);
                _context5.t1 = _context5.t0.response.status;
                _context5.next = _context5.t1 === 422 ? 33 : _context5.t1 === 500 ? 35 : 37;
                break;

              case 33:
                this.errors = _context5.t0.response.data.errors;
                return _context5.abrupt("break", 38);

              case 35:
                this.flashMessage.error({
                  message: _context5.t0.response.data.message,
                  time: 5000
                });
                return _context5.abrupt("break", 38);

              case 37:
                this.flashMessage.error({
                  message: _context5.t0.response.data.message,
                  time: 5000
                });

              case 38:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[18, 28]]);
      }));

      function createTenant() {
        return _createTenant.apply(this, arguments);
      }

      return createTenant;
    }(),
    hideEditTenantModel: function hideEditTenantModel() {
      this.$refs.editTenantModel.hide();
    },
    showEditTenantModel: function showEditTenantModel() {
      this.$refs.editTenantModel.show();
    },
    editTenant: function editTenant(tenant) {
      this.editTenantData = tenant;
      this.showEditTenantModel();
    },
    updateTenant: function () {
      var _updateTenant = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                if (this.editTenantData.name_en) {
                  _context6.next = 4;
                  break;
                }

                this.editError = "Name EN required.";
                _context6.next = 37;
                break;

              case 4:
                if (this.editTenantData.name_ar) {
                  _context6.next = 8;
                  break;
                }

                this.editError = 'Name AR required.';
                _context6.next = 37;
                break;

              case 8:
                if (this.editTenantData.building_id) {
                  _context6.next = 12;
                  break;
                }

                this.editError = 'Building required.';
                _context6.next = 37;
                break;

              case 12:
                this.editError = null;
                formData = new FormData();
                formData.append('name_en', this.editTenantData.name_en);
                formData.append('name_ar', this.editTenantData.name_ar);
                formData.append('building_id', this.editTenantData.building_id);
                formData.append('_method', 'put');
                _context6.prev = 18;
                _context6.next = 21;
                return _services_tenant_service__WEBPACK_IMPORTED_MODULE_1__["updateTenant"](this.editTenantData.id, formData);

              case 21:
                response = _context6.sent;
                this.tenants.data.map(function (tenant) {
                  if (tenant.id === response.data.data.id) {
                    for (var key in response.data.data) {
                      tenant[key] = response.data.data[key];
                    }
                  }
                });
                this.hideEditTenantModel();
                this.flashMessage.success({
                  title: 'Tenant Successfully Updated'
                });
                _context6.next = 37;
                break;

              case 27:
                _context6.prev = 27;
                _context6.t0 = _context6["catch"](18);
                _context6.t1 = _context6.t0.response.status;
                _context6.next = _context6.t1 === 422 ? 32 : _context6.t1 === 500 ? 34 : 36;
                break;

              case 32:
                this.flashMessage.error({
                  message: _context6.t0.response.data.errors,
                  time: 5000
                });
                return _context6.abrupt("break", 37);

              case 34:
                this.flashMessage.error({
                  message: _context6.t0.response.data.message,
                  time: 5000
                });
                return _context6.abrupt("break", 37);

              case 36:
                this.flashMessage.error({
                  message: _context6.t0.response.data.message,
                  time: 5000
                });

              case 37:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[18, 27]]);
      }));

      function updateTenant() {
        return _updateTenant.apply(this, arguments);
      }

      return updateTenant;
    }(),
    updateButton: function () {
      var _updateButton = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var _this = this;

        var formData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                formData = new FormData();
                formData.append('update', this.update);
                formData.append('tenantIds', this.checkedTenantId);
                this.$confirm({
                  title: 'Are you sure?',
                  message: "you want to ".concat(this.update, " ?"),
                  button: {
                    no: 'No',
                    yes: 'Yes'
                  },

                  /**
                   * Callback Function
                   * @param {Boolean} confirm
                   */
                  callback: function callback(confirm) {
                    if (confirm) {
                      try {
                        _services_tenant_service__WEBPACK_IMPORTED_MODULE_1__["activeTenant"](formData); // this.tenants.data = this.tenants.data.filter(obj => {
                        //     return obj.id !== tenant.id;
                        // });

                        _this.flashMessage.success({
                          title: "Tenant Successfully ".concat(_this.update)
                        });
                      } catch (error) {
                        _this.flashMessage.error({
                          message: error.response.data.message,
                          time: 5000
                        });
                      }
                    }
                  }
                });

              case 4:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function updateButton() {
        return _updateButton.apply(this, arguments);
      }

      return updateButton;
    }(),
    deleteTenant: function () {
      var _deleteTenant = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8(tenant) {
        var _this2 = this;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                this.$confirm({
                  title: 'Are you sure?',
                  message: "you want to delete ".concat(tenant.name_en, " (").concat(tenant.name_ar, ")?"),
                  button: {
                    no: 'No',
                    yes: 'Yes'
                  },

                  /**
                   * Callback Function
                   * @param {Boolean} confirm
                   */
                  callback: function callback(confirm) {
                    if (confirm) {
                      try {
                        _services_tenant_service__WEBPACK_IMPORTED_MODULE_1__["deleteTenant"](tenant.id);
                        _this2.tenants.data = _this2.tenants.data.filter(function (obj) {
                          return obj.id !== tenant.id;
                        });
                      } catch (error) {
                        _this2.flashMessage.error({
                          message: error.response.data.message,
                          time: 5000
                        });
                      }
                    }
                  }
                });

              case 1:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function deleteTenant(_x) {
        return _deleteTenant.apply(this, arguments);
      }

      return deleteTenant;
    }()
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Tenant.vue?vue&type=template&id=57013d89&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Tenant.vue?vue&type=template&id=57013d89&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-xl-12 col-lg-12" },
        [
          _c(
            "div",
            {
              staticClass:
                "card-header py-3 d-flex flex-row align-items-center justify-content-between"
            },
            [
              _c("b-col", { attrs: { cols: "10" } }, [
                _c("h6", { staticClass: "m-0 font-weight-bold text-primary" }, [
                  _vm._v("Create Tenant")
                ])
              ]),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "2" } },
                [
                  _c(
                    "b-button",
                    {
                      attrs: { id: "show-btn", variant: "primary" },
                      on: { click: _vm.showNewTenantModel }
                    },
                    [_vm._v("Create Tenant")]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card shadow py-3 mb-4" },
            [
              _c(
                "b-row",
                { staticClass: "px-5" },
                [
                  _c(
                    "b-col",
                    { attrs: { cols: "11", md: "6" } },
                    [
                      _c(
                        "b-form-group",
                        {
                          attrs: {
                            "label-cols": "3",
                            "label-cols-lg": "2",
                            label: "Update",
                            "label-for": "Update"
                          }
                        },
                        [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.update,
                                  expression: "update"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { id: "update" },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.update = $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "Active" } }, [
                                _vm._v("Active")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Deactive" } }, [
                                _vm._v("Deactive")
                              ])
                            ]
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { cols: "1", md: "1" } },
                    [
                      _c("b-button", { on: { click: _vm.updateButton } }, [
                        _vm._v("apply")
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { cols: "12", md: "6" } },
                    [
                      _c(
                        "b-form-group",
                        {
                          attrs: {
                            "label-cols": "4",
                            "label-cols-lg": "2",
                            label: "Filter by Building",
                            "label-for": "filter"
                          }
                        },
                        [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.filterSelect,
                                  expression: "filterSelect"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { id: "filter" },
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.filterSelect = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  },
                                  _vm.filterby
                                ]
                              }
                            },
                            [
                              _c("option", { attrs: { disabled: "" } }, [
                                _vm._v(" Select Building")
                              ]),
                              _vm._v(" "),
                              _vm._l(_vm.buildings, function(building) {
                                return _c(
                                  "option",
                                  { domProps: { value: building.name } },
                                  [
                                    _vm._v(
                                      "\n                                        " +
                                        _vm._s(building.name) +
                                        "\n                                    "
                                    )
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { cols: "12", md: "6" } },
                    [
                      _c("b-form-input", {
                        attrs: { placeholder: "Search" },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.search($event)
                          }
                        },
                        model: {
                          value: _vm.search,
                          callback: function($$v) {
                            _vm.search = $$v
                          },
                          expression: "search"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c("div", { staticClass: "table-responsive" }, [
                  _c(
                    "table",
                    {
                      staticClass: "table table-bordered",
                      attrs: {
                        id: "dataTable",
                        width: "100%",
                        cellspacing: "0"
                      }
                    },
                    [
                      _c("thead", [
                        _c("tr", [
                          _c("th"),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Name EN "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("name_en")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Name AR "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("name_ar")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Active/Deactive "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("active")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("th", [_vm._v("Building")]),
                          _vm._v(" "),
                          _c("td", [_vm._v("Action")])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "tbody",
                        _vm._l(_vm.tenants.data, function(tenant) {
                          return _vm.tenants.data
                            ? _c("tr", [
                                _c(
                                  "td",
                                  [
                                    _c("b-form-checkbox", {
                                      attrs: { size: "sm", value: tenant.id },
                                      model: {
                                        value: _vm.checkedTenantId,
                                        callback: function($$v) {
                                          _vm.checkedTenantId = $$v
                                        },
                                        expression: "checkedTenantId"
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(tenant.name_en))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(tenant.name_ar))]),
                                _vm._v(" "),
                                _c("td", [
                                  tenant.active == 1
                                    ? _c("p", [_vm._v("Active")])
                                    : _c("p", [_vm._v("Deactive")])
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(tenant.building.name))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-info btn-circle btn-sm",
                                      on: {
                                        click: function($event) {
                                          return _vm.editTenant(tenant)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-edit" })]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-danger btn-circle btn-sm",
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteTenant(tenant)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-trash" })]
                                  )
                                ])
                              ])
                            : _vm._e()
                        }),
                        0
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "mt-3 text-center" },
                    [
                      _c("pagination", {
                        attrs: { data: _vm.tenants },
                        on: { "pagination-change-page": _vm.getTenants }
                      })
                    ],
                    1
                  )
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "newTenantModel",
              attrs: { "hide-footer": "", title: "Create New Tenant" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.createTenant($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c(
                      "p",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.error,
                            expression: "error"
                          }
                        ],
                        staticStyle: { color: "red" }
                      },
                      [
                        _vm._v(
                          "\n                                * " +
                            _vm._s(_vm.error)
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "name_en" } }, [
                        _vm._v("Name EN")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.tenantData.name_en,
                            expression: "tenantData.name_en"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "name_en" },
                        domProps: { value: _vm.tenantData.name_en },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.tenantData,
                              "name_en",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "name_ar" } }, [
                        _vm._v("Name AR")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.tenantData.name_ar,
                            expression: "tenantData.name_ar"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "name_ar" },
                        domProps: { value: _vm.tenantData.name_ar },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.tenantData,
                              "name_ar",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "building" } }, [
                        _vm._v("Building")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.tenantData.building_id,
                              expression: "tenantData.building_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "building" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.tenantData,
                                "building_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.buildings, function(building) {
                          return _c(
                            "option",
                            { domProps: { value: building.id } },
                            [
                              _vm._v(
                                "\n                                        " +
                                  _vm._s(building.name) +
                                  "\n                                    "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideNewTenantModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Save")]
                      )
                    ])
                  ])
                ]
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "editTenantModel",
              attrs: { "hide-footer": "", title: "Create Edit Tenant" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.updateTenant($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c(
                      "p",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.editError,
                            expression: "editError"
                          }
                        ],
                        staticStyle: { color: "red" }
                      },
                      [
                        _vm._v(
                          "\n                                * " +
                            _vm._s(_vm.editError)
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "edit_name_en" } }, [
                        _vm._v("Name EN")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editTenantData.name_en,
                            expression: "editTenantData.name_en"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "edit_name_en" },
                        domProps: { value: _vm.editTenantData.name_en },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editTenantData,
                              "name_en",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "edit_name_ar" } }, [
                        _vm._v("Name AR")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editTenantData.name_ar,
                            expression: "editTenantData.name_ar"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "edit_name_ar" },
                        domProps: { value: _vm.editTenantData.name_ar },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editTenantData,
                              "name_ar",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "building_edit" } }, [
                        _vm._v("Building")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.editTenantData.building_id,
                              expression: "editTenantData.building_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "building_edit" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.editTenantData,
                                "building_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.buildings, function(building) {
                          return _c(
                            "option",
                            { domProps: { value: building.id } },
                            [
                              _vm._v(
                                "\n                                        " +
                                  _vm._s(building.name) +
                                  "\n                                    "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideEditTenantModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Update")]
                      )
                    ])
                  ])
                ]
              )
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "d-sm-flex align-items-center justify-content-between mb-4"
      },
      [_c("h1", { staticClass: "h3 mb-0 text-gray-800" }, [_vm._v("Tenants")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/services/tenant_service.js":
/*!*************************************************!*\
  !*** ./resources/js/services/tenant_service.js ***!
  \*************************************************/
/*! exports provided: createTenant, deleteTenant, updateTenant, getTenant, getBuilding, activeTenant */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createTenant", function() { return createTenant; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteTenant", function() { return deleteTenant; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateTenant", function() { return updateTenant; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTenant", function() { return getTenant; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBuilding", function() { return getBuilding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "activeTenant", function() { return activeTenant; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createTenant(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/tenants', data);
}
function deleteTenant(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/tenants/".concat(id));
}
function updateTenant(id, data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post("/tenants/".concat(id), data);
}
function getTenant(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/tenants', {
    params: params
  });
}
function getBuilding() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/buildings');
}
function activeTenant(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/activeTenants', params);
}

/***/ }),

/***/ "./resources/js/views/Tenant.vue":
/*!***************************************!*\
  !*** ./resources/js/views/Tenant.vue ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Tenant_vue_vue_type_template_id_57013d89_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Tenant.vue?vue&type=template&id=57013d89&scoped=true& */ "./resources/js/views/Tenant.vue?vue&type=template&id=57013d89&scoped=true&");
/* harmony import */ var _Tenant_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Tenant.vue?vue&type=script&lang=js& */ "./resources/js/views/Tenant.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Tenant_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Tenant_vue_vue_type_template_id_57013d89_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Tenant_vue_vue_type_template_id_57013d89_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "57013d89",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Tenant.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Tenant.vue?vue&type=script&lang=js&":
/*!****************************************************************!*\
  !*** ./resources/js/views/Tenant.vue?vue&type=script&lang=js& ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tenant_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Tenant.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Tenant.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Tenant_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Tenant.vue?vue&type=template&id=57013d89&scoped=true&":
/*!**********************************************************************************!*\
  !*** ./resources/js/views/Tenant.vue?vue&type=template&id=57013d89&scoped=true& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tenant_vue_vue_type_template_id_57013d89_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Tenant.vue?vue&type=template&id=57013d89&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Tenant.vue?vue&type=template&id=57013d89&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tenant_vue_vue_type_template_id_57013d89_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Tenant_vue_vue_type_template_id_57013d89_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);