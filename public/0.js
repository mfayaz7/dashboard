(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Order.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_order_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/order_service */ "./resources/js/services/order_service.js");
/* harmony import */ var _services_booking_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/booking_service */ "./resources/js/services/booking_service.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Order",
  data: function data() {
    return {
      orders: {},
      allSelected: false,
      orderData: [],
      checkedOrderId: [],
      update: null,
      search: null,
      editOrderData: {},
      errors: {},
      filterSelect: null,
      tenants: [],
      products: [],
      direction: 'asc',
      sort: null,
      page: 1,
      reason: null
    };
  },
  created: function created() {
    this.getOrders();
    this.getTenants();
    this.getProducts();
  },
  watch: {
    search: function search(val, old) {
      if (val === "") {
        this.getOrders();
      } else {
        this.searchData();
      }
    }
  },
  methods: {
    hideNewOrderModel: function hideNewOrderModel() {
      this.$refs.newOrderModel.hide();
    },
    showNewOrderModel: function showNewOrderModel() {
      this.$refs.newOrderModel.show();
    },
    sortChange: function sortChange(key) {
      this.direction = this.sort !== key || this.direction === 'desc' ? 'asc' : 'desc';
      this.sort = key;
      this.getOrders(1);
    },
    filterby: function () {
      var _filterby = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.getOrders(1);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function filterby() {
        return _filterby.apply(this, arguments);
      }

      return filterby;
    }(),
    searchData: function () {
      var _searchData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this.getOrders(1);

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function searchData() {
        return _searchData.apply(this, arguments);
      }

      return searchData;
    }(),
    getOrders: function () {
      var _getOrders = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var page,
            params,
            response,
            _args3 = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                page = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : 1;
                params = {
                  page: page
                };

                if (this.sort !== null) {
                  params['sort'] = this.sort;
                  params['direction'] = this.direction;
                }

                if (this.search !== null) params['search'] = this.search;
                if (this.filterSelect !== null) params['filter'] = this.filterSelect;
                _context3.prev = 5;
                _context3.next = 8;
                return _services_order_service__WEBPACK_IMPORTED_MODULE_1__["getOrder"](params);

              case 8:
                response = _context3.sent;
                this.orders = response.data;
                _context3.next = 15;
                break;

              case 12:
                _context3.prev = 12;
                _context3.t0 = _context3["catch"](5);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 15:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[5, 12]]);
      }));

      function getOrders() {
        return _getOrders.apply(this, arguments);
      }

      return getOrders;
    }(),
    getProducts: function () {
      var _getProducts = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _services_order_service__WEBPACK_IMPORTED_MODULE_1__["getProducts"]();

              case 3:
                response = _context4.sent;
                this.products = response.data.data; // console.log(response.data.data);

                _context4.next = 10;
                break;

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 7]]);
      }));

      function getProducts() {
        return _getProducts.apply(this, arguments);
      }

      return getProducts;
    }(),
    getTenants: function () {
      var _getTenants = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _services_booking_service__WEBPACK_IMPORTED_MODULE_2__["getTenant"]();

              case 3:
                response = _context5.sent;
                this.tenants = response.data.data; // console.log(response.data.data);

                _context5.next = 10;
                break;

              case 7:
                _context5.prev = 7;
                _context5.t0 = _context5["catch"](0);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 10:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[0, 7]]);
      }));

      function getTenants() {
        return _getTenants.apply(this, arguments);
      }

      return getTenants;
    }(),
    createOrder: function () {
      var _createOrder = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var product_id, tenant_id, quantity, formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                product_id = '';
                tenant_id = '';
                quantity = '';
                if (typeof this.orderData.product_id !== "undefined") product_id = this.orderData.product_id;
                if (typeof this.orderData.tenant_id !== "undefined") tenant_id = this.orderData.tenant_id;
                if (typeof this.orderData.quantity !== "undefined") quantity = this.orderData.quantity;
                formData = new FormData();
                formData.append('product_id', product_id);
                formData.append('tenant_id', tenant_id);
                formData.append('quantity', quantity);
                _context6.prev = 10;
                _context6.next = 13;
                return _services_order_service__WEBPACK_IMPORTED_MODULE_1__["createOrder"](formData);

              case 13:
                response = _context6.sent;
                this.errors = {};
                this.orders.data.unshift(response.data);
                this.hideNewOrderModel();
                this.flashMessage.success({
                  title: 'Order Successfully Created'
                });
                _context6.next = 30;
                break;

              case 20:
                _context6.prev = 20;
                _context6.t0 = _context6["catch"](10);
                _context6.t1 = _context6.t0.response.status;
                _context6.next = _context6.t1 === 422 ? 25 : _context6.t1 === 500 ? 27 : 29;
                break;

              case 25:
                this.errors = _context6.t0.response.data.errors;
                return _context6.abrupt("break", 30);

              case 27:
                this.flashMessage.error({
                  message: _context6.t0.response.data.message,
                  time: 5000
                });
                return _context6.abrupt("break", 30);

              case 29:
                this.flashMessage.error({
                  message: _context6.t0.response.data.message,
                  time: 5000
                });

              case 30:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[10, 20]]);
      }));

      function createOrder() {
        return _createOrder.apply(this, arguments);
      }

      return createOrder;
    }(),
    hideRejectOrderModel: function hideRejectOrderModel() {
      this.$refs.rejectOrderModel.hide();
    },
    showRejectOrderModel: function showRejectOrderModel() {
      this.$refs.rejectOrderModel.show();
    },
    updateButton: function () {
      var _updateButton = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var formData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                formData = new FormData();
                formData.append('update', this.update);
                formData.append('orderIds', this.checkedOrderId);

                if (this.update == 'Rejected') {
                  this.showRejectOrderModel();
                } else {
                  this.updateOrder(formData);
                }

              case 4:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function updateButton() {
        return _updateButton.apply(this, arguments);
      }

      return updateButton;
    }(),
    updateOrder: function () {
      var _updateOrder = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8(formData) {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _context8.next = 3;
                return _services_order_service__WEBPACK_IMPORTED_MODULE_1__["updateOrder"](formData);

              case 3:
                response = _context8.sent;
                this.orders.data.map(function (order) {
                  if (order.id === response.data.id) {
                    for (var key in response.data) {
                      order[key] = response.data[key];
                    }
                  }
                });
                this.flashMessage.success({
                  title: 'Order Successfully Updated'
                });
                _context8.next = 18;
                break;

              case 8:
                _context8.prev = 8;
                _context8.t0 = _context8["catch"](0);
                _context8.t1 = _context8.t0.response.status;
                _context8.next = _context8.t1 === 422 ? 13 : _context8.t1 === 500 ? 15 : 17;
                break;

              case 13:
                this.flashMessage.error({
                  message: _context8.t0.response.data.errors,
                  time: 5000
                });
                return _context8.abrupt("break", 18);

              case 15:
                this.flashMessage.error({
                  message: _context8.t0.response.data.message,
                  time: 5000
                });
                return _context8.abrupt("break", 18);

              case 17:
                this.flashMessage.error({
                  message: _context8.t0.response.data.message,
                  time: 5000
                });

              case 18:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this, [[0, 8]]);
      }));

      function updateOrder(_x) {
        return _updateOrder.apply(this, arguments);
      }

      return updateOrder;
    }(),
    rejectOrder: function () {
      var _rejectOrder = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var formData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                formData = new FormData();
                formData.append('update', this.update);
                formData.append('orderIds', this.checkedOrderId);
                formData.append('reason', this.reason);
                this.updateOrder(formData);
                this.hideRejectOrderModel();

              case 6:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, this);
      }));

      function rejectOrder() {
        return _rejectOrder.apply(this, arguments);
      }

      return rejectOrder;
    }()
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.order-status .New[data-v-621856bf] {\n    background: #c6e1c6;\n    color: #5b841b;\n    padding: 5px 15px;\n    border-radius: 5px;\n}\n.order-status .Ongoing[data-v-621856bf] {\n    background: #E9AC24;\n    color: #c1e3bc;\n    padding: 5px 15px;\n    border-radius: 5px;\n}\n.order-status .Cancelled[data-v-621856bf] {\n    background: #b3b3b3;\n    color: #ffffff;\n    padding: 5px 15px;\n    border-radius: 5px;\n}\n.order-status .Rejected[data-v-621856bf] {\n    background: #e90005;\n    color: #c1e3bc;\n    padding: 5px 15px;\n    border-radius: 5px;\n}\n.order-status .Completed[data-v-621856bf] {\n    background: #2b8022;\n    color: #c1e3bc;\n    padding: 5px 15px;\n    border-radius: 5px;\n}\n\n\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=template&id=621856bf&scoped=true&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Order.vue?vue&type=template&id=621856bf&scoped=true& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-xl-12 col-lg-12" },
        [
          _c(
            "div",
            {
              staticClass:
                "card-header py-3 d-flex flex-row align-items-center justify-content-between"
            },
            [
              _c("b-col", { attrs: { cols: "10" } }, [
                _c("h6", { staticClass: "m-0 font-weight-bold text-primary" }, [
                  _vm._v("Create Order")
                ])
              ]),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "2" } },
                [
                  _c(
                    "b-button",
                    {
                      attrs: { id: "show-btn", variant: "primary" },
                      on: { click: _vm.showNewOrderModel }
                    },
                    [_vm._v("Create Order")]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card shadow py-3 mb-4" },
            [
              _c(
                "b-row",
                { staticClass: "px-5" },
                [
                  _c(
                    "b-col",
                    { attrs: { cols: "11", md: "6" } },
                    [
                      _c(
                        "b-form-group",
                        {
                          attrs: {
                            "label-cols": "3",
                            "label-cols-lg": "2",
                            label: "Update",
                            "label-for": "Update"
                          }
                        },
                        [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.update,
                                  expression: "update"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { id: "update" },
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.update = $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                }
                              }
                            },
                            [
                              _c("option", { attrs: { value: "New" } }, [
                                _vm._v("Change status to New")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Ongoing" } }, [
                                _vm._v("Change status to Ongoing")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Completed" } }, [
                                _vm._v("Change status to Completed")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Cancelled" } }, [
                                _vm._v("Change status to Cancelled")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Rejected" } }, [
                                _vm._v("Change status to Rejected")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Delete" } }, [
                                _vm._v("Delete")
                              ])
                            ]
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { cols: "1", md: "1" } },
                    [
                      _c("b-button", { on: { click: _vm.updateButton } }, [
                        _vm._v("apply")
                      ])
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { cols: "12", md: "6" } },
                    [
                      _c(
                        "b-form-group",
                        {
                          attrs: {
                            "label-cols": "3",
                            "label-cols-lg": "2",
                            label: "Filter by Status",
                            "label-for": "filter"
                          }
                        },
                        [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.filterSelect,
                                  expression: "filterSelect"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { id: "filter" },
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.filterSelect = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  },
                                  _vm.filterby
                                ]
                              }
                            },
                            [
                              _c("option", { attrs: { disabled: "" } }, [
                                _vm._v(" Select Status")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "New" } }, [
                                _vm._v("New")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Ongoing" } }, [
                                _vm._v("Ongoing")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Completed" } }, [
                                _vm._v("Completed")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Cancelled" } }, [
                                _vm._v("Cancelled")
                              ]),
                              _vm._v(" "),
                              _c("option", { attrs: { value: "Rejected" } }, [
                                _vm._v("Rejected")
                              ])
                            ]
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { cols: "12", md: "6" } },
                    [
                      _c("b-form-input", {
                        attrs: { placeholder: "Search" },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.search($event)
                          }
                        },
                        model: {
                          value: _vm.search,
                          callback: function($$v) {
                            _vm.search = $$v
                          },
                          expression: "search"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c("div", { staticClass: "table-responsive" }, [
                  _c(
                    "table",
                    {
                      staticClass: "table table-bordered",
                      attrs: {
                        id: "dataTable",
                        width: "100%",
                        cellspacing: "0"
                      }
                    },
                    [
                      _c("thead", [
                        _c("tr", [
                          _c("th"),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Order ID "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("id")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("th", [_vm._v("Product ")]),
                          _vm._v(" "),
                          _c("th", [_vm._v("Tenant")]),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Quantity "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("quantity")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("th", [_vm._v("Total Amount ")]),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Status "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("status")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("th", [_vm._v("Reason")])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "tbody",
                        _vm._l(_vm.orders.data, function(order, index) {
                          return _vm.orders.data
                            ? _c("tr", { key: index }, [
                                _c(
                                  "td",
                                  [
                                    _c("b-form-checkbox", {
                                      attrs: { size: "sm", value: order.id },
                                      model: {
                                        value: _vm.checkedOrderId,
                                        callback: function($$v) {
                                          _vm.checkedOrderId = $$v
                                        },
                                        expression: "checkedOrderId"
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(order.id))]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(order.product.name_en))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(order.tenant.name_en))
                                ]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(order.quantity))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(order.total_amount))]),
                                _vm._v(" "),
                                _c("td", { staticClass: "order-status" }, [
                                  _c("span", { class: order.status }, [
                                    _vm._v(_vm._s(order.status))
                                  ])
                                ]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(order.reason))])
                              ])
                            : _vm._e()
                        }),
                        0
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "mt-3 text-center" },
                    [
                      _c("pagination", {
                        attrs: { data: _vm.orders },
                        on: { "pagination-change-page": _vm.getOrders }
                      })
                    ],
                    1
                  )
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "newOrderModel",
              attrs: { "hide-footer": "", title: "Create New Order" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.createOrder($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "product" } }, [
                        _vm._v("Product")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.orderData.product_id,
                              expression: "orderData.product_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "product" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.orderData,
                                "product_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.products, function(product) {
                          return _c(
                            "option",
                            { domProps: { value: product.id } },
                            [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(product.name_en) +
                                  "\n                                "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "tenant" } }, [
                        _vm._v("Tenant")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.orderData.tenant_id,
                              expression: "orderData.tenant_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "tenant" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.orderData,
                                "tenant_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.tenants, function(tenant) {
                          return _c(
                            "option",
                            { domProps: { value: tenant.id } },
                            [
                              _vm._v(
                                "\n                                    " +
                                  _vm._s(tenant.name_en) +
                                  "\n                                "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "quantity" } }, [
                        _vm._v("Quantity")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.orderData.quantity,
                            expression: "orderData.quantity"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "number", id: "quantity" },
                        domProps: { value: _vm.orderData.quantity },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.orderData,
                              "quantity",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.quantity
                        ? _c("div", { staticClass: "invalid-feedback" }, [
                            _vm._v(_vm._s(_vm.errors.quantity[0]))
                          ])
                        : _vm._e()
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideNewOrderModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Save")]
                      )
                    ])
                  ])
                ]
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "rejectOrderModel",
              attrs: { "hide-footer": "", title: "Create Edit Order" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.rejectOrder($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "reason" } }, [
                        _vm._v("Reason")
                      ]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.reason,
                            expression: "reason"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "reason", rows: "3" },
                        domProps: { value: _vm.reason },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.reason = $event.target.value
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideRejectOrderModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Submit to Reject")]
                      )
                    ])
                  ])
                ]
              )
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "d-sm-flex align-items-center justify-content-between mb-4"
      },
      [_c("h1", { staticClass: "h3 mb-0 text-gray-800" }, [_vm._v("Orders")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/services/booking_service.js":
/*!**************************************************!*\
  !*** ./resources/js/services/booking_service.js ***!
  \**************************************************/
/*! exports provided: createBooking, deleteBooking, updateBooking, getBooking, getTenant, getService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createBooking", function() { return createBooking; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteBooking", function() { return deleteBooking; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateBooking", function() { return updateBooking; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBooking", function() { return getBooking; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTenant", function() { return getTenant; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getService", function() { return getService; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createBooking(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/bookings', data);
}
function deleteBooking(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/bookings/".concat(id));
}
function updateBooking(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/updateBookings', params);
}
function getBooking(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/bookings', {
    params: params
  });
}
function getTenant() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/tenants');
}
function getService() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/services');
}

/***/ }),

/***/ "./resources/js/services/order_service.js":
/*!************************************************!*\
  !*** ./resources/js/services/order_service.js ***!
  \************************************************/
/*! exports provided: createOrder, deleteOrder, getOrder, getBuilding, getProducts, getTenants, updateOrder */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createOrder", function() { return createOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteOrder", function() { return deleteOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getOrder", function() { return getOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBuilding", function() { return getBuilding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProducts", function() { return getProducts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTenants", function() { return getTenants; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateOrder", function() { return updateOrder; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createOrder(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/orders', data);
}
function deleteOrder(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/orders/".concat(id));
} // export function updateOrder(id,data) {
//     return http().post(`/orders/${id}`,data);
// }

function getOrder(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/orders', {
    params: params
  });
}
function getBuilding() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/buildings');
}
function getProducts() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/products');
}
function getTenants() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/tenants');
}
function updateOrder(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/updateOrders', params);
}

/***/ }),

/***/ "./resources/js/views/Order.vue":
/*!**************************************!*\
  !*** ./resources/js/views/Order.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Order_vue_vue_type_template_id_621856bf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Order.vue?vue&type=template&id=621856bf&scoped=true& */ "./resources/js/views/Order.vue?vue&type=template&id=621856bf&scoped=true&");
/* harmony import */ var _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Order.vue?vue&type=script&lang=js& */ "./resources/js/views/Order.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Order_vue_vue_type_style_index_0_id_621856bf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css& */ "./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Order_vue_vue_type_template_id_621856bf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Order_vue_vue_type_template_id_621856bf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "621856bf",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Order.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Order.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/views/Order.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css& ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_621856bf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader??ref--6-1!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--6-2!../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=style&index=0&id=621856bf&scoped=true&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_621856bf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_621856bf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_621856bf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_style_index_0_id_621856bf_scoped_true_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/views/Order.vue?vue&type=template&id=621856bf&scoped=true&":
/*!*********************************************************************************!*\
  !*** ./resources/js/views/Order.vue?vue&type=template&id=621856bf&scoped=true& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_621856bf_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Order.vue?vue&type=template&id=621856bf&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Order.vue?vue&type=template&id=621856bf&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_621856bf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Order_vue_vue_type_template_id_621856bf_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);