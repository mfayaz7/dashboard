(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Products.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_product_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/product_service */ "./resources/js/services/product_service.js");
/* harmony import */ var _services_category_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/category_service */ "./resources/js/services/category_service.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Product",
  data: function data() {
    return {
      products: {},
      categories: [],
      productData: [],
      search: null,
      editProductData: [],
      errors: null,
      editError: null,
      direction: 'asc',
      sort: null,
      page: 1
    };
  },
  created: function created() {
    this.getProducts();
    this.getCategories();
  },
  watch: {
    search: function search(val, old) {
      if (val === "") {
        this.getProducts();
      } else {
        this.searchData();
      }
    }
  },
  methods: {
    hideNewProductModel: function hideNewProductModel() {
      this.$refs.newProductModel.hide();
    },
    showNewProductModel: function showNewProductModel() {
      this.$refs.newProductModel.show();
    },
    sortChange: function sortChange(key) {
      this.direction = this.sort !== key || this.direction === 'desc' ? 'asc' : 'desc';
      this.sort = key;
      this.getProducts(1);
    },
    searchData: function () {
      var _searchData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.getProducts(1);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function searchData() {
        return _searchData.apply(this, arguments);
      }

      return searchData;
    }(),
    getCategories: function () {
      var _getCategories = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _services_category_service__WEBPACK_IMPORTED_MODULE_2__["getCategories"]();

              case 3:
                response = _context2.sent;
                this.categories = response.data.data;
                _context2.next = 10;
                break;

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](0);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[0, 7]]);
      }));

      function getCategories() {
        return _getCategories.apply(this, arguments);
      }

      return getCategories;
    }(),
    getProducts: function () {
      var _getProducts = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var page,
            params,
            response,
            _args3 = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                page = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : 1;
                params = {
                  page: page
                };

                if (this.sort !== null) {
                  params['sort'] = this.sort;
                  params['direction'] = this.direction;
                }

                if (this.search !== null) params['search'] = this.search;
                _context3.prev = 4;
                _context3.next = 7;
                return _services_product_service__WEBPACK_IMPORTED_MODULE_1__["getProduct"](params);

              case 7:
                response = _context3.sent;
                this.products = response.data;
                _context3.next = 14;
                break;

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](4);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[4, 11]]);
      }));

      function getProducts() {
        return _getProducts.apply(this, arguments);
      }

      return getProducts;
    }(),
    createProduct: function () {
      var _createProduct = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (this.productData.name_en) {
                  _context4.next = 4;
                  break;
                }

                this.errors = "Name EN required.";
                _context4.next = 42;
                break;

              case 4:
                if (this.productData.name_ar) {
                  _context4.next = 8;
                  break;
                }

                this.errors = 'Name AR required.';
                _context4.next = 42;
                break;

              case 8:
                if (this.productData.price) {
                  _context4.next = 12;
                  break;
                }

                this.errors = "Price required.";
                _context4.next = 42;
                break;

              case 12:
                if (this.productData.category_id) {
                  _context4.next = 16;
                  break;
                }

                this.errors = 'Category required.';
                _context4.next = 42;
                break;

              case 16:
                this.errors = null;
                formData = new FormData();
                formData.append('name_en', this.productData.name_en);
                formData.append('name_ar', this.productData.name_ar);
                formData.append('price', this.productData.price);
                formData.append('category_id', this.productData.category_id);
                _context4.prev = 22;
                _context4.next = 25;
                return _services_product_service__WEBPACK_IMPORTED_MODULE_1__["createProduct"](formData);

              case 25:
                response = _context4.sent;
                this.errors = {};
                this.products.data.unshift(response.data.data);
                this.hideNewProductModel();
                this.flashMessage.success({
                  title: 'Product Successfully Created'
                });
                _context4.next = 42;
                break;

              case 32:
                _context4.prev = 32;
                _context4.t0 = _context4["catch"](22);
                _context4.t1 = _context4.t0.response.status;
                _context4.next = _context4.t1 === 422 ? 37 : _context4.t1 === 500 ? 39 : 41;
                break;

              case 37:
                this.errors = _context4.t0.response.data.errors;
                return _context4.abrupt("break", 42);

              case 39:
                this.flashMessage.error({
                  message: _context4.t0.response.data.message,
                  time: 5000
                });
                return _context4.abrupt("break", 42);

              case 41:
                this.flashMessage.error({
                  message: _context4.t0.response.data.message,
                  time: 5000
                });

              case 42:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[22, 32]]);
      }));

      function createProduct() {
        return _createProduct.apply(this, arguments);
      }

      return createProduct;
    }(),
    hideEditProductModel: function hideEditProductModel() {
      this.$refs.editProductModel.hide();
    },
    showEditProductModel: function showEditProductModel() {
      this.$refs.editProductModel.show();
    },
    editProduct: function editProduct(product) {
      this.editProductData = product;
      this.showEditProductModel();
    },
    updateProduct: function () {
      var _updateProduct = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (this.editProductData.name_en) {
                  _context5.next = 4;
                  break;
                }

                this.editError = "Name EN required.";
                _context5.next = 42;
                break;

              case 4:
                if (this.editProductData.name_ar) {
                  _context5.next = 8;
                  break;
                }

                this.editError = 'Name AR required.';
                _context5.next = 42;
                break;

              case 8:
                if (this.editProductData.price) {
                  _context5.next = 12;
                  break;
                }

                this.editError = "Price required.";
                _context5.next = 42;
                break;

              case 12:
                if (this.editProductData.category_id) {
                  _context5.next = 16;
                  break;
                }

                this.editError = 'Category required.';
                _context5.next = 42;
                break;

              case 16:
                this.editError = null;
                formData = new FormData();
                formData.append('name_en', this.editProductData.name_en);
                formData.append('name_ar', this.editProductData.name_ar);
                formData.append('price', this.editProductData.price);
                formData.append('category_id', this.editProductData.category_id);
                formData.append('_method', 'put');
                _context5.prev = 23;
                _context5.next = 26;
                return _services_product_service__WEBPACK_IMPORTED_MODULE_1__["updateProduct"](this.editProductData.id, formData);

              case 26:
                response = _context5.sent;
                this.products.data.map(function (product) {
                  if (product.id === response.data.data.id) {
                    for (var key in response.data.data) {
                      product[key] = response.data.data[key];
                    }
                  }
                });
                this.hideEditProductModel();
                this.flashMessage.success({
                  title: 'Product Successfully Updated'
                });
                _context5.next = 42;
                break;

              case 32:
                _context5.prev = 32;
                _context5.t0 = _context5["catch"](23);
                _context5.t1 = _context5.t0.response.status;
                _context5.next = _context5.t1 === 422 ? 37 : _context5.t1 === 500 ? 39 : 41;
                break;

              case 37:
                this.flashMessage.error({
                  message: _context5.t0.response.data.errors,
                  time: 5000
                });
                return _context5.abrupt("break", 42);

              case 39:
                this.flashMessage.error({
                  message: _context5.t0.response.data.message,
                  time: 5000
                });
                return _context5.abrupt("break", 42);

              case 41:
                this.flashMessage.error({
                  message: _context5.t0.response.data.message,
                  time: 5000
                });

              case 42:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[23, 32]]);
      }));

      function updateProduct() {
        return _updateProduct.apply(this, arguments);
      }

      return updateProduct;
    }(),
    deleteProduct: function () {
      var _deleteProduct = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6(product) {
        var _this = this;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                this.$confirm({
                  title: 'Are you sure?',
                  message: "you want to delete ".concat(product.name_en, " (").concat(product.name_ar, ")?"),
                  button: {
                    no: 'No',
                    yes: 'Yes'
                  },

                  /**
                   * Callback Function
                   * @param {Boolean} confirm
                   */
                  callback: function callback(confirm) {
                    if (confirm) {
                      try {
                        _services_product_service__WEBPACK_IMPORTED_MODULE_1__["deleteProduct"](product.id);
                        _this.products.data = _this.products.data.filter(function (obj) {
                          return obj.id !== product.id;
                        });
                      } catch (error) {
                        _this.flashMessage.error({
                          message: error.response.data.message,
                          time: 5000
                        });
                      }
                    }
                  }
                });

              case 1:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function deleteProduct(_x) {
        return _deleteProduct.apply(this, arguments);
      }

      return deleteProduct;
    }()
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-xl-12 col-lg-12" },
        [
          _c(
            "div",
            {
              staticClass:
                "card-header py-3 d-flex flex-row align-items-center justify-content-between"
            },
            [
              _c("b-col", { attrs: { cols: "10" } }, [
                _c("h6", { staticClass: "m-0 font-weight-bold text-primary" }, [
                  _vm._v("Create Product")
                ])
              ]),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "2" } },
                [
                  _c(
                    "b-button",
                    {
                      attrs: { id: "show-btn", variant: "primary" },
                      on: { click: _vm.showNewProductModel }
                    },
                    [_vm._v("Create Product")]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card shadow py-3 mb-4" },
            [
              _c(
                "b-row",
                { staticClass: "px-5" },
                [
                  _c(
                    "b-col",
                    { attrs: { cols: "4" } },
                    [
                      _c("b-form-input", {
                        attrs: { placeholder: "Search" },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.search($event)
                          }
                        },
                        model: {
                          value: _vm.search,
                          callback: function($$v) {
                            _vm.search = $$v
                          },
                          expression: "search"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c("div", { staticClass: "table-responsive" }, [
                  _c(
                    "table",
                    {
                      staticClass: "table table-bordered",
                      attrs: {
                        id: "dataTable",
                        width: "100%",
                        cellspacing: "0"
                      }
                    },
                    [
                      _c("thead", [
                        _c("tr", [
                          _c(
                            "th",
                            [
                              _vm._v("Name EN "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("name_en")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Name AR "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("name_ar")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Price "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("price")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("th", [_vm._v("Category")]),
                          _vm._v(" "),
                          _c("td", [_vm._v("Action")])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "tbody",
                        _vm._l(_vm.products.data, function(product, index) {
                          return _vm.products.data
                            ? _c("tr", { key: index }, [
                                _c("td", [_vm._v(_vm._s(product.name_en))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(product.name_ar))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(product.price))]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(product.category.name_en))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-info btn-circle btn-sm",
                                      on: {
                                        click: function($event) {
                                          return _vm.editProduct(product)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-edit" })]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-danger btn-circle btn-sm",
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteProduct(product)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-trash" })]
                                  )
                                ])
                              ])
                            : _vm._e()
                        }),
                        0
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "mt-3 text-center" },
                    [
                      _c("pagination", {
                        attrs: { data: _vm.products },
                        on: { "pagination-change-page": _vm.getProducts }
                      })
                    ],
                    1
                  )
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "newProductModel",
              attrs: { "hide-footer": "", title: "create new product" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.createProduct($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c(
                      "p",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.errors,
                            expression: "errors"
                          }
                        ],
                        staticStyle: { color: "red" }
                      },
                      [
                        _vm._v(
                          "\n                            * " +
                            _vm._s(_vm.errors)
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "name_en" } }, [
                        _vm._v("Name EN")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.productData.name_en,
                            expression: "productData.name_en"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "name_en" },
                        domProps: { value: _vm.productData.name_en },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.productData,
                              "name_en",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "name_ar" } }, [
                        _vm._v("Name AR")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.productData.name_ar,
                            expression: "productData.name_ar"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "name_ar" },
                        domProps: { value: _vm.productData.name_ar },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.productData,
                              "name_ar",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "price" } }, [
                        _vm._v("Price")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.productData.price,
                            expression: "productData.price"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "number", min: "0", id: "price" },
                        domProps: { value: _vm.productData.price },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.productData,
                              "price",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "category" } }, [
                        _vm._v("Category")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.productData.category_id,
                              expression: "productData.category_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "category" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.productData,
                                "category_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.categories, function(category) {
                          return _c(
                            "option",
                            { domProps: { value: category.id } },
                            [
                              _vm._v(
                                "\n                                        " +
                                  _vm._s(category.name_en) +
                                  "\n                                    "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideNewProductModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Save")]
                      )
                    ])
                  ])
                ]
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "editProductModel",
              attrs: { "hide-footer": "", title: "create edit product" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.updateProduct($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c(
                      "p",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.editError,
                            expression: "editError"
                          }
                        ],
                        staticStyle: { color: "red" }
                      },
                      [
                        _vm._v(
                          "\n                                * " +
                            _vm._s(_vm.editError)
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "edit_name_en" } }, [
                        _vm._v("Name EN")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editProductData.name_en,
                            expression: "editProductData.name_en"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "edit_name_en" },
                        domProps: { value: _vm.editProductData.name_en },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editProductData,
                              "name_en",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "edit_name_ar" } }, [
                        _vm._v("Name AR")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editProductData.name_ar,
                            expression: "editProductData.name_ar"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "edit_name_ar" },
                        domProps: { value: _vm.editProductData.name_ar },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editProductData,
                              "name_ar",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "price_edit" } }, [
                        _vm._v("Price")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editProductData.price,
                            expression: "editProductData.price"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "number", min: "0", id: "price_edit" },
                        domProps: { value: _vm.editProductData.price },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editProductData,
                              "price",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "category_edit" } }, [
                        _vm._v("Category")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.editProductData.category_id,
                              expression: "editProductData.category_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "category_edit" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.editProductData,
                                "category_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.categories, function(category) {
                          return _c(
                            "option",
                            { domProps: { value: category.id } },
                            [
                              _vm._v(
                                "\n                                        " +
                                  _vm._s(category.name_en) +
                                  "\n                                    "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideEditProductModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Update")]
                      )
                    ])
                  ])
                ]
              )
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "d-sm-flex align-items-center justify-content-between mb-4"
      },
      [_c("h1", { staticClass: "h3 mb-0 text-gray-800" }, [_vm._v("Products")])]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/services/category_service.js":
/*!***************************************************!*\
  !*** ./resources/js/services/category_service.js ***!
  \***************************************************/
/*! exports provided: createCategory, deleteCategory, updateCategory, getCategory, getCategories */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createCategory", function() { return createCategory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteCategory", function() { return deleteCategory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCategory", function() { return updateCategory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCategory", function() { return getCategory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCategories", function() { return getCategories; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createCategory(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/categories', data);
}
function deleteCategory(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/categories/".concat(id));
}
function updateCategory(id, data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post("/categories/".concat(id), data);
}
function getCategory(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/categories', {
    params: params
  });
}
function getCategories() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/categories');
}

/***/ }),

/***/ "./resources/js/services/product_service.js":
/*!**************************************************!*\
  !*** ./resources/js/services/product_service.js ***!
  \**************************************************/
/*! exports provided: createProduct, deleteProduct, updateProduct, getProduct */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createProduct", function() { return createProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteProduct", function() { return deleteProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateProduct", function() { return updateProduct; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProduct", function() { return getProduct; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createProduct(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/products', data);
}
function deleteProduct(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/products/".concat(id));
}
function updateProduct(id, data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post("/products/".concat(id), data);
}
function getProduct(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/products', {
    params: params
  });
}

/***/ }),

/***/ "./resources/js/views/Products.vue":
/*!*****************************************!*\
  !*** ./resources/js/views/Products.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Products.vue?vue&type=template&id=eec6f8fa&scoped=true& */ "./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&");
/* harmony import */ var _Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Products.vue?vue&type=script&lang=js& */ "./resources/js/views/Products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "eec6f8fa",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Products.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Products.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/views/Products.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Products.vue?vue&type=template&id=eec6f8fa&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Products.vue?vue&type=template&id=eec6f8fa&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Products_vue_vue_type_template_id_eec6f8fa_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);