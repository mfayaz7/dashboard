(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[3],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Building.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Building.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _services_building_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../services/building_service */ "./resources/js/services/building_service.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/user_service */ "./resources/js/services/user_service.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Building",
  data: function data() {
    return {
      buildings: {},
      buildingData: [],
      properties: [],
      search: null,
      editBuildingData: {},
      error: null,
      editError: null,
      filterSelect: null,
      direction: 'asc',
      sort: null,
      page: 1
    };
  },
  mounted: function mounted() {
    this.getBuildings();
    this.getProperties();
  },
  watch: {
    search: function search(val, old) {
      if (val === "") {
        this.getBuildings();
      } else {
        this.searchData();
      }
    }
  },
  methods: {
    hideNewBuildingModel: function hideNewBuildingModel() {
      this.$refs.newBuildingModel.hide();
    },
    showNewBuildingModel: function showNewBuildingModel() {
      this.$refs.newBuildingModel.show();
    },
    sortChange: function sortChange(key) {
      this.direction = this.sort !== key || this.direction === 'desc' ? 'asc' : 'desc';
      this.sort = key;
      this.getBuildings(1);
    },
    filterby: function () {
      var _filterby = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.getBuildings(1);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function filterby() {
        return _filterby.apply(this, arguments);
      }

      return filterby;
    }(),
    searchData: function () {
      var _searchData = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this.getBuildings(1);

              case 1:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function searchData() {
        return _searchData.apply(this, arguments);
      }

      return searchData;
    }(),
    getBuildings: function () {
      var _getBuildings = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var page,
            params,
            response,
            _args3 = arguments;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                page = _args3.length > 0 && _args3[0] !== undefined ? _args3[0] : 1;
                params = {
                  page: page
                };

                if (this.sort !== null) {
                  params['sort'] = this.sort;
                  params['direction'] = this.direction;
                }

                if (this.search !== null) params['search'] = this.search;
                if (this.filterSelect !== null) params['filter'] = this.filterSelect;
                _context3.prev = 5;
                _context3.next = 8;
                return _services_building_service__WEBPACK_IMPORTED_MODULE_1__["getBuilding"](params);

              case 8:
                response = _context3.sent;
                this.buildings = response.data;
                _context3.next = 15;
                break;

              case 12:
                _context3.prev = 12;
                _context3.t0 = _context3["catch"](5);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 15:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[5, 12]]);
      }));

      function getBuildings() {
        return _getBuildings.apply(this, arguments);
      }

      return getBuildings;
    }(),
    getProperties: function () {
      var _getProperties = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _services_building_service__WEBPACK_IMPORTED_MODULE_1__["getProperty"]();

              case 3:
                response = _context4.sent;
                this.properties = response.data.data;
                console.log(response.data.data);
                _context4.next = 11;
                break;

              case 8:
                _context4.prev = 8;
                _context4.t0 = _context4["catch"](0);
                this.flashMessage.error({
                  message: 'Some error occured',
                  time: 5000
                });

              case 11:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this, [[0, 8]]);
      }));

      function getProperties() {
        return _getProperties.apply(this, arguments);
      }

      return getProperties;
    }(),
    createBuilding: function () {
      var _createBuilding = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                if (this.buildingData.name) {
                  _context5.next = 4;
                  break;
                }

                this.error = "Name required.";
                _context5.next = 42;
                break;

              case 4:
                if (this.buildingData.city) {
                  _context5.next = 8;
                  break;
                }

                this.error = 'City required.';
                _context5.next = 42;
                break;

              case 8:
                if (this.buildingData.address) {
                  _context5.next = 12;
                  break;
                }

                this.error = 'Address required.';
                _context5.next = 42;
                break;

              case 12:
                if (this.buildingData.property_id) {
                  _context5.next = 16;
                  break;
                }

                this.error = 'Property required.';
                _context5.next = 42;
                break;

              case 16:
                this.error = null;
                formData = new FormData();
                formData.append('name', this.buildingData.name);
                formData.append('city', this.buildingData.city);
                formData.append('address', this.buildingData.address);
                formData.append('property_id', this.buildingData.property_id);
                _context5.prev = 22;
                _context5.next = 25;
                return _services_building_service__WEBPACK_IMPORTED_MODULE_1__["createBuilding"](formData);

              case 25:
                response = _context5.sent;
                this.errors = {};
                this.buildings.data.unshift(response.data.data);
                this.hideNewBuildingModel();
                this.flashMessage.success({
                  title: 'Building Successfully Created'
                });
                _context5.next = 42;
                break;

              case 32:
                _context5.prev = 32;
                _context5.t0 = _context5["catch"](22);
                _context5.t1 = _context5.t0.response.status;
                _context5.next = _context5.t1 === 422 ? 37 : _context5.t1 === 500 ? 39 : 41;
                break;

              case 37:
                this.errors = _context5.t0.response.data.errors;
                return _context5.abrupt("break", 42);

              case 39:
                this.flashMessage.error({
                  message: _context5.t0.response.data.message,
                  time: 5000
                });
                return _context5.abrupt("break", 42);

              case 41:
                this.flashMessage.error({
                  message: _context5.t0.response.data.message,
                  time: 5000
                });

              case 42:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this, [[22, 32]]);
      }));

      function createBuilding() {
        return _createBuilding.apply(this, arguments);
      }

      return createBuilding;
    }(),
    hideEditBuildingModel: function hideEditBuildingModel() {
      this.$refs.editBuildingModel.hide();
    },
    showEditBuildingModel: function showEditBuildingModel() {
      this.$refs.editBuildingModel.show();
    },
    editBuilding: function editBuilding(building) {
      this.editBuildingData = building;
      this.showEditBuildingModel();
    },
    updateBuilding: function () {
      var _updateBuilding = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var formData, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                if (this.editBuildingData.name) {
                  _context6.next = 4;
                  break;
                }

                this.editError = "Name required.";
                _context6.next = 42;
                break;

              case 4:
                if (this.editBuildingData.city) {
                  _context6.next = 8;
                  break;
                }

                this.editError = 'City required.';
                _context6.next = 42;
                break;

              case 8:
                if (this.editBuildingData.address) {
                  _context6.next = 12;
                  break;
                }

                this.editError = 'Address required.';
                _context6.next = 42;
                break;

              case 12:
                if (this.editBuildingData.property_id) {
                  _context6.next = 16;
                  break;
                }

                this.editError = 'Property required.';
                _context6.next = 42;
                break;

              case 16:
                this.editError = null;
                formData = new FormData();
                formData.append('name', this.editBuildingData.name);
                formData.append('city', this.editBuildingData.city);
                formData.append('address', this.editBuildingData.address);
                formData.append('property_id', this.editBuildingData.property_id);
                formData.append('_method', 'put');
                _context6.prev = 23;
                _context6.next = 26;
                return _services_building_service__WEBPACK_IMPORTED_MODULE_1__["updateBuilding"](this.editBuildingData.id, formData);

              case 26:
                response = _context6.sent;
                this.buildings.data.map(function (building) {
                  if (building.id === response.data.data.id) {
                    for (var key in response.data.data) {
                      building[key] = response.data.data[key];
                    }
                  }
                });
                this.hideEditBuildingModel();
                this.flashMessage.success({
                  title: 'Building Successfully Updated'
                });
                _context6.next = 42;
                break;

              case 32:
                _context6.prev = 32;
                _context6.t0 = _context6["catch"](23);
                _context6.t1 = _context6.t0.response.status;
                _context6.next = _context6.t1 === 422 ? 37 : _context6.t1 === 500 ? 39 : 41;
                break;

              case 37:
                this.flashMessage.error({
                  message: _context6.t0.response.data.errors,
                  time: 5000
                });
                return _context6.abrupt("break", 42);

              case 39:
                this.flashMessage.error({
                  message: _context6.t0.response.data.message,
                  time: 5000
                });
                return _context6.abrupt("break", 42);

              case 41:
                this.flashMessage.error({
                  message: _context6.t0.response.data.message,
                  time: 5000
                });

              case 42:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[23, 32]]);
      }));

      function updateBuilding() {
        return _updateBuilding.apply(this, arguments);
      }

      return updateBuilding;
    }(),
    deleteBuilding: function () {
      var _deleteBuilding = _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7(building) {
        var _this = this;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                this.$confirm({
                  title: 'Are you sure?',
                  message: "you want to delete ".concat(building.name_en, " (").concat(building.name_ar, ")?"),
                  button: {
                    no: 'No',
                    yes: 'Yes'
                  },

                  /**
                   * Callback Function
                   * @param {Boolean} confirm
                   */
                  callback: function callback(confirm) {
                    if (confirm) {
                      try {
                        _services_building_service__WEBPACK_IMPORTED_MODULE_1__["deleteBuilding"](building.id);
                        _this.buildings.data = _this.buildings.data.filter(function (obj) {
                          return obj.id !== building.id;
                        });
                      } catch (error) {
                        _this.flashMessage.error({
                          message: error.response.data.message,
                          time: 5000
                        });
                      }
                    }
                  }
                });

              case 1:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function deleteBuilding(_x) {
        return _deleteBuilding.apply(this, arguments);
      }

      return deleteBuilding;
    }()
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Building.vue?vue&type=template&id=10899cd3&scoped=true&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/Building.vue?vue&type=template&id=10899cd3&scoped=true& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "container-fluid" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "row" }, [
      _c(
        "div",
        { staticClass: "col-xl-12 col-lg-12" },
        [
          _c(
            "div",
            {
              staticClass:
                "card-header py-3 d-flex flex-row align-items-center justify-content-between"
            },
            [
              _c("b-col", { attrs: { cols: "10" } }, [
                _c("h6", { staticClass: "m-0 font-weight-bold text-primary" }, [
                  _vm._v("Create Building")
                ])
              ]),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "2" } },
                [
                  _c(
                    "b-button",
                    {
                      attrs: { id: "show-btn", variant: "primary" },
                      on: { click: _vm.showNewBuildingModel }
                    },
                    [_vm._v("Create Building")]
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "card shadow py-3 mb-4" },
            [
              _c(
                "b-row",
                { staticClass: "px-5" },
                [
                  _c(
                    "b-col",
                    { attrs: { cols: "4" } },
                    [
                      _c(
                        "b-form-group",
                        {
                          attrs: {
                            "label-cols": "4",
                            "label-cols-lg": "2",
                            label: "Filter by Property",
                            "label-for": "filter"
                          }
                        },
                        [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.filterSelect,
                                  expression: "filterSelect"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: { id: "filter" },
                              on: {
                                change: [
                                  function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.filterSelect = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  },
                                  _vm.filterby
                                ]
                              }
                            },
                            [
                              _c("option", { attrs: { disabled: "" } }, [
                                _vm._v(" Select Property")
                              ]),
                              _vm._v(" "),
                              _vm._l(_vm.properties, function(property) {
                                return _c(
                                  "option",
                                  { domProps: { value: property.name_en } },
                                  [
                                    _vm._v(
                                      "\n                                        " +
                                        _vm._s(property.name_en) +
                                        "\n                                    "
                                    )
                                  ]
                                )
                              })
                            ],
                            2
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { attrs: { cols: "8" } },
                    [
                      _c("b-form-input", {
                        attrs: { placeholder: "Search" },
                        on: {
                          keyup: function($event) {
                            if (
                              !$event.type.indexOf("key") &&
                              _vm._k(
                                $event.keyCode,
                                "enter",
                                13,
                                $event.key,
                                "Enter"
                              )
                            ) {
                              return null
                            }
                            return _vm.search($event)
                          }
                        },
                        model: {
                          value: _vm.search,
                          callback: function($$v) {
                            _vm.search = $$v
                          },
                          expression: "search"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c("div", { staticClass: "card-body" }, [
                _c("div", { staticClass: "table-responsive" }, [
                  _c(
                    "table",
                    {
                      staticClass: "table table-bordered",
                      attrs: {
                        id: "dataTable",
                        width: "100%",
                        cellspacing: "0"
                      }
                    },
                    [
                      _c("thead", [
                        _c("tr", [
                          _c(
                            "th",
                            [
                              _vm._v("Name "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("name")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("City "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("city")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "th",
                            [
                              _vm._v("Address "),
                              _c("b-icon", {
                                staticStyle: { cursor: "pointer" },
                                attrs: {
                                  icon: "arrow-down-up",
                                  variant: "dark"
                                },
                                on: {
                                  click: function($event) {
                                    return _vm.sortChange("address")
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("th", [_vm._v("Property ")]),
                          _vm._v(" "),
                          _c("td", [_vm._v("Action")])
                        ])
                      ]),
                      _vm._v(" "),
                      _c(
                        "tbody",
                        _vm._l(_vm.buildings.data, function(building) {
                          return _vm.buildings.data
                            ? _c("tr", [
                                _c("td", [_vm._v(_vm._s(building.name))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(building.city))]),
                                _vm._v(" "),
                                _c("td", [_vm._v(_vm._s(building.address))]),
                                _vm._v(" "),
                                _c("td", [
                                  _vm._v(_vm._s(building.property.name_en))
                                ]),
                                _vm._v(" "),
                                _c("td", [
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-info btn-circle btn-sm",
                                      on: {
                                        click: function($event) {
                                          return _vm.editBuilding(building)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-edit" })]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "button",
                                    {
                                      staticClass:
                                        "btn btn-danger btn-circle btn-sm",
                                      on: {
                                        click: function($event) {
                                          return _vm.deleteBuilding(building)
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fas fa-trash" })]
                                  )
                                ])
                              ])
                            : _vm._e()
                        }),
                        0
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "mt-3 text-center" },
                    [
                      _c("pagination", {
                        attrs: { data: _vm.buildings },
                        on: { "pagination-change-page": _vm.getBuildings }
                      })
                    ],
                    1
                  )
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "newBuildingModel",
              attrs: { "hide-footer": "", title: "Create New Building" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.createBuilding($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c(
                      "p",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.error,
                            expression: "error"
                          }
                        ],
                        staticStyle: { color: "red" }
                      },
                      [
                        _vm._v(
                          "\n                                * " +
                            _vm._s(_vm.error)
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "name" } }, [_vm._v("Name")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.buildingData.name,
                            expression: "buildingData.name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "name" },
                        domProps: { value: _vm.buildingData.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.buildingData,
                              "name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "city" } }, [_vm._v("City")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.buildingData.city,
                            expression: "buildingData.city"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "city" },
                        domProps: { value: _vm.buildingData.city },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.buildingData,
                              "city",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "address" } }, [
                        _vm._v("Address")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.buildingData.address,
                            expression: "buildingData.address"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "address" },
                        domProps: { value: _vm.buildingData.address },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.buildingData,
                              "address",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "property" } }, [
                        _vm._v("Property")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.buildingData.property_id,
                              expression: "buildingData.property_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "property" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.buildingData,
                                "property_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.properties, function(property) {
                          return _c(
                            "option",
                            { domProps: { value: property.id } },
                            [
                              _vm._v(
                                "\n                                        " +
                                  _vm._s(property.name_en) +
                                  "\n                                    "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideNewBuildingModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Save")]
                      )
                    ])
                  ])
                ]
              )
            ]
          ),
          _vm._v(" "),
          _c(
            "b-modal",
            {
              ref: "editBuildingModel",
              attrs: { "hide-footer": "", title: "Create Edit Building" }
            },
            [
              _c(
                "div",
                {
                  staticClass: "form",
                  staticStyle: { margin: "10px" },
                  on: {
                    submit: function($event) {
                      $event.preventDefault()
                      return _vm.updateBuilding($event)
                    }
                  }
                },
                [
                  _c("form", [
                    _c(
                      "p",
                      {
                        directives: [
                          {
                            name: "show",
                            rawName: "v-show",
                            value: _vm.editError,
                            expression: "editError"
                          }
                        ],
                        staticStyle: { color: "red" }
                      },
                      [
                        _vm._v(
                          "\n                                * " +
                            _vm._s(_vm.editError)
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "edit_name" } }, [
                        _vm._v("Name")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editBuildingData.name,
                            expression: "editBuildingData.name"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "edit_name" },
                        domProps: { value: _vm.editBuildingData.name },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editBuildingData,
                              "name",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "edit_city" } }, [
                        _vm._v("City")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editBuildingData.city,
                            expression: "editBuildingData.city"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "edit_city" },
                        domProps: { value: _vm.editBuildingData.city },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editBuildingData,
                              "city",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "edit_address" } }, [
                        _vm._v("Address")
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.editBuildingData.address,
                            expression: "editBuildingData.address"
                          }
                        ],
                        staticClass: "form-control",
                        attrs: { type: "text", id: "edit_address" },
                        domProps: { value: _vm.editBuildingData.address },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.editBuildingData,
                              "address",
                              $event.target.value
                            )
                          }
                        }
                      })
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", { attrs: { for: "property_edit" } }, [
                        _vm._v("Property")
                      ]),
                      _vm._v(" "),
                      _c(
                        "select",
                        {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: _vm.editBuildingData.property_id,
                              expression: "editBuildingData.property_id"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { id: "property_edit" },
                          on: {
                            change: function($event) {
                              var $$selectedVal = Array.prototype.filter
                                .call($event.target.options, function(o) {
                                  return o.selected
                                })
                                .map(function(o) {
                                  var val = "_value" in o ? o._value : o.value
                                  return val
                                })
                              _vm.$set(
                                _vm.editBuildingData,
                                "property_id",
                                $event.target.multiple
                                  ? $$selectedVal
                                  : $$selectedVal[0]
                              )
                            }
                          }
                        },
                        _vm._l(_vm.properties, function(property) {
                          return _c(
                            "option",
                            { domProps: { value: property.id } },
                            [
                              _vm._v(
                                "\n                                        " +
                                  _vm._s(property.name_en) +
                                  "\n                                    "
                              )
                            ]
                          )
                        }),
                        0
                      )
                    ]),
                    _vm._v(" "),
                    _c("hr"),
                    _vm._v(" "),
                    _c("div", { staticClass: "text-right" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-default",
                          attrs: { type: "button" },
                          on: { click: _vm.hideEditBuildingModel }
                        },
                        [_vm._v("Cancel")]
                      ),
                      _vm._v(" "),
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-success",
                          attrs: { type: "submit" }
                        },
                        [_vm._v("Update")]
                      )
                    ])
                  ])
                ]
              )
            ]
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      {
        staticClass: "d-sm-flex align-items-center justify-content-between mb-4"
      },
      [
        _c("h1", { staticClass: "h3 mb-0 text-gray-800" }, [
          _vm._v("Buildings")
        ])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/services/building_service.js":
/*!***************************************************!*\
  !*** ./resources/js/services/building_service.js ***!
  \***************************************************/
/*! exports provided: createBuilding, deleteBuilding, updateBuilding, getBuilding, getProperty */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createBuilding", function() { return createBuilding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteBuilding", function() { return deleteBuilding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateBuilding", function() { return updateBuilding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getBuilding", function() { return getBuilding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProperty", function() { return getProperty; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createBuilding(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/buildings', data);
}
function deleteBuilding(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/buildings/".concat(id));
}
function updateBuilding(id, data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post("/buildings/".concat(id), data);
}
function getBuilding(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/buildings', {
    params: params
  });
}
function getProperty() {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/properties');
}

/***/ }),

/***/ "./resources/js/services/user_service.js":
/*!***********************************************!*\
  !*** ./resources/js/services/user_service.js ***!
  \***********************************************/
/*! exports provided: createUser, deleteUser, updateUser, getUsers, activeUser */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "createUser", function() { return createUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteUser", function() { return deleteUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateUser", function() { return updateUser; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUsers", function() { return getUsers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "activeUser", function() { return activeUser; });
/* harmony import */ var _http_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./http_service */ "./resources/js/services/http_service.js");

function createUser(data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/users', data);
}
function deleteUser(id) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])()["delete"]("/users/".concat(id));
}
function updateUser(id, data) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post("/users/".concat(id), data);
}
function getUsers(params) {
  console.log('dsdsdsds');
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().get('/users', {
    params: params
  });
}
function activeUser(params) {
  return Object(_http_service__WEBPACK_IMPORTED_MODULE_0__["http"])().post('/activeUsers', params);
}

/***/ }),

/***/ "./resources/js/views/Building.vue":
/*!*****************************************!*\
  !*** ./resources/js/views/Building.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Building_vue_vue_type_template_id_10899cd3_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Building.vue?vue&type=template&id=10899cd3&scoped=true& */ "./resources/js/views/Building.vue?vue&type=template&id=10899cd3&scoped=true&");
/* harmony import */ var _Building_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Building.vue?vue&type=script&lang=js& */ "./resources/js/views/Building.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Building_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Building_vue_vue_type_template_id_10899cd3_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Building_vue_vue_type_template_id_10899cd3_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "10899cd3",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/Building.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/Building.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/views/Building.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Building_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Building.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Building.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Building_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/Building.vue?vue&type=template&id=10899cd3&scoped=true&":
/*!************************************************************************************!*\
  !*** ./resources/js/views/Building.vue?vue&type=template&id=10899cd3&scoped=true& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Building_vue_vue_type_template_id_10899cd3_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Building.vue?vue&type=template&id=10899cd3&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/Building.vue?vue&type=template&id=10899cd3&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Building_vue_vue_type_template_id_10899cd3_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Building_vue_vue_type_template_id_10899cd3_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);