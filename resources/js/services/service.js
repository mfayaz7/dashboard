import {http} from './http_service'

export function createService(data) {
    return http().post('/services',data);
}

export function deleteService(id) {
    return http().delete(`/services/${id}`);
}

export function updateService(id,data) {
    return http().post(`/services/${id}`,data);
}

export function getService(params) {
    return http().get('/services',{params:params});
}

