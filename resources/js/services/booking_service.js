import {http} from './http_service'

export function createBooking(data) {
    return http().post('/bookings',data);
}

export function deleteBooking(id) {
    return http().delete(`/bookings/${id}`);
}

export function updateBooking(params) {
    return http().post('/updateBookings' ,params);
}

export function getBooking(params) {
    return http().get('/bookings',{params:params});
}

export function getTenant() {
    return http().get('/tenants');
}
export function getService() {
    return http().get('/services');
}

