import {http} from './http_service'

export function createOrder(data) {
    return http().post('/orders',data);
}

export function deleteOrder(id) {
    return http().delete(`/orders/${id}`);
}

// export function updateOrder(id,data) {
//     return http().post(`/orders/${id}`,data);
// }

export function getOrder(params) {
    return http().get('/orders' ,{params:params});
}


export function getBuilding() {
    return http().get('/buildings');
}


export function getProducts() {
    return http().get('/products');
}

export function getTenants() {
    return http().get('/tenants');
}

export function updateOrder(params) {
    return http().post('/updateOrders' ,params);
}
