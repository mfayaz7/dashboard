import {http,httpFile} from './http_service'

export function createProperty(data) {
    return httpFile().post('/properties',data);
}

export function deleteProperty(id) {
    return http().delete(`/properties/${id}`);
}

export function updateProperty(id,data) {
    return httpFile().post(`/properties/${id}`,data);
}

export function getProperty(params) {
    return http().get('/properties' ,  {params:params});
}

