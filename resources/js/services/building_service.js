import {http} from './http_service'

export function createBuilding(data) {
    return http().post('/buildings',data);
}

export function deleteBuilding(id) {
    return http().delete(`/buildings/${id}`);
}

export function updateBuilding(id,data) {
    return http().post(`/buildings/${id}`,data);
}

export function getBuilding(params) {
    return http().get('/buildings',{params:params});
}


export function getProperty() {
    return http().get('/properties' );
}

