import {http} from './http_service'

export function createTenant(data) {
    return http().post('/tenants',data);
}

export function deleteTenant(id) {
    return http().delete(`/tenants/${id}`);
}

export function updateTenant(id,data) {
    return http().post(`/tenants/${id}`,data);
}

export function getTenant(params) {
    return http().get('/tenants',{params:params});
}


export function getBuilding() {
    return http().get('/buildings');
}


export function activeTenant(params) {
    return http().post('/activeTenants' ,params);
}

