<?php
return [
    'status' => [
        'NEW' => "New",
        'ONGOING' => "Ongoing",
        'COMPLETED' => "Completed",
        'CANCELLED' => "Cancelled",
        'REJECTED' => "Rejected",
    ]
];
