<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::group(['prefix' => 'auth'],function (){
    Route::post('register',[AuthController::class,'register']);
    Route::post('login',[AuthController::class,'login']);

    Route::group(['middleware'=>'auth:api'],function (){
        Route::get('logout',[AuthController::class,'logout']);
        Route::get('profile',[AuthController::class,'profile']);
    });
});

Route::group(['middleware' => 'auth:api'],function () {
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('categories', 'CategoryController');
    Route::resource('services', 'ServiceController');
    Route::resource('products', 'ProductController');
    Route::resource('properties', 'PropertyController');
    Route::resource('buildings', 'BuildingController');
    Route::resource('tenants', 'TenantController');
    Route::resource('orders', 'OrderController');
    Route::post('/updateOrders', 'OrderController@updateOrders');
    Route::post('/updateBookings', 'BookingController@updateBookings');
    Route::post('/activeTenants', 'TenantController@activeTenant');
    Route::post('/activeUsers', 'UserController@activeUser');
    Route::resource('bookings', 'BookingController');
});
