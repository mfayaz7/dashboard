<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'service_id','tenant_id', 'booking_date','total_amount', 'status'
    ];

    public function service(){
        return $this->belongsTo(Service::class);
    }

    public function tenant(){
        return $this->belongsTo(Tenant::class);
    }
}
