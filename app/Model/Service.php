<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_en','name_ar', 'price','category_id','reason'
    ];

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
