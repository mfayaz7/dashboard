<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_en','name_ar', 'active','building_id'
    ];

    public function building(){
        return $this->belongsTo(Building::class);
    }
}
