<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id','tenant_id', 'quantity','total_amount', 'status','reason'
    ];

    public function product(){
        return $this->belongsTo(Product::class);
    }
    public function tenant(){
        return $this->belongsTo(Tenant::class);
    }
}
