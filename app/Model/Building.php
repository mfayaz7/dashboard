<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','city', 'address','property_id'
    ];

    public function property(){
        return $this->belongsTo(Property::class);
    }
}
