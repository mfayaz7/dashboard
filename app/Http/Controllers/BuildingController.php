<?php

namespace App\Http\Controllers;

use App\Http\Resources\BuildingResource;
use App\Model\Building;
use App\Model\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BuildingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        Log::info('indexxx');
        $query=Building::query();
        if ($request->has('search')) {
            $query->where('name', 'like', '%' . $request->get('search') . '%')
                ->orWhere('city', 'like', '%' . $request->get('search') . '%')
                ->orWhere('address', 'like', '%' . $request->get('search') . '%');
        }

        if ($request->has('filter')) {
            $query->whereHas('property', function ($query) use ($request) {
                return $query->where('name_en', $request->get('filter'));
            });
        }
        if ($request->has('sort')){
            $buildings = $query->with('property')->orderBy($request->get('sort') ,$request->get('direction') )->paginate(5);
        }
        else
            $buildings = $query->with('property')->orderBy('id','ASC')->paginate(5);
        return response()->json( $buildings,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Log::info('store');

        $request->validate([
            'name' => 'required|min:3|max:255',
            'city' => 'required|min:1|max:255',
            'address' => 'required',
            'property_id' => 'required',
        ]);

        $property=Property::find($request->input('property_id'));
        $building = Building::create([
            'name' => $request->input('name'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'property_id' => $property->id,
        ]);

        return new BuildingResource($building);
//        return response()->json( $building,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Building  $building
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Building $building, Request $request)
    {
        Log::info('update.........');
        $validator = Validator::make($request->all(),[
            'name' => 'required|min:3|max:255',
            'city' => 'required|min:1|max:255',
            'address' => 'required',
            'property_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }
        $property=Property::find($request->input('property_id'));
        $building->update([
            'name' => $request->input('name'),
            'city' => $request->input('city'),
            'address' => $request->input('address'),
            'property_id' => $property->id
        ]);

        return new BuildingResource($building);
//        return response()->json( $building,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Building $building
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Building $building)
    {
        Log::info('delete');
        if($building->delete()){
            return response()->json([
                'message' => 'Building delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
