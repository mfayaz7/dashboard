<?php

namespace App\Http\Controllers;

use App\Model\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query=Property::query();
        if ($request->has('search')) {
            $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%');
        }
        if ($request->has('sort')){
            $properties = $query->orderBy($request->get('sort') ,$request->get('direction') )->paginate(5);
        }
        else
            $properties = $query->orderBy('id','ASC')->paginate(5);
        return response()->json( $properties,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:3|max:255',
            'property_img' => 'required|image|mimes:jpeg,png,jpg'
        ]);


        $file = $request->file('property_img');
        $path = $file->hashName('property_img');
        $image = Image::make($file);
        $image->fit(250, 250, function ($constraint) {
            $constraint->aspectRatio();
        });

        Storage::put($path, (string) $image->encode());
        $property = Property::create([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'property_img' => $path,
        ]);

        return response()->json( $property,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Property  $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Property $property, Request $request)
    {
        Log::info('update.........');
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
        ]);

        $property->name_en =$request->name_en;
        $property->name_ar =$request->name_ar;
        $oldPath=$property->property_img;

        if($request->hasFile('property_img')){
            $request->validate([
               'property_img'=>'image|mimes:jpeg,png,jpg'
            ]);

            Storage::delete($oldPath);
            $file = $request->file('property_img');
            $path = $file->hashName('property_img');
            $image = Image::make($file);
            $image->fit(250, 250, function ($constraint) {
                $constraint->aspectRatio();
            });

            Storage::put($path, (string) $image->encode());
            $property->property_img=$path;

        }

        if ($property->save()){
            return response()->json($property,200);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Property $property
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Property $property)
    {
        Log::info('delete');
        if($property->delete()){
            return response()->json([
                'message' => 'Property delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
