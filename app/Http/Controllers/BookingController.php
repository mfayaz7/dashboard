<?php

namespace App\Http\Controllers;

use App\Model\Booking;
use App\Model\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query=Booking::query();
        if ($request->has('search')) {
            $query->with('service')->with('tenant');
            $query->whereHas('service', function ($query) use ($request) {
                $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                    ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%');
            })
                ->orWhereHas('tenant', function ($query) use ($request) {
                    $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                        ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%');
                });
        }

        if ($request->has('filter')) {
            $query->where('status', $request->get('filter'));
        }
        if ($request->has('sort')){
            $bookings = $query->with(['service','tenant'])->orderBy($request->get('sort') ,$request->get('direction') )->paginate(5);
        }
        else
            $bookings = $query->with(['service','tenant'])->orderBy('id','ASC')->paginate(5);

        return response()->json( $bookings,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Log::info('store booking');

        $request->validate([
            'service_id' => 'required',
            'tenant_id' => 'required',
            'booking_date' => 'required',
        ]);
        $inputDate = strtotime($request->input('booking_date'));
        $bookingDate = date('Y-m-d',$inputDate);
        $service=Service::find($request->input('service_id'));
        $totalAmount=$service->price;
        $booking = Booking::create([
            'service_id' => $request->input('service_id'),
            'tenant_id' => $request->input('tenant_id'),
            'booking_date' => $bookingDate,
            'total_amount' => $totalAmount,
            'status' => $request->input('status'),
        ]);
        return response()->json( $booking,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Booking  $booking
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Booking $booking, Request $request)
    {
        Log::info('update.........');
        $validator = Validator::make($request->all(),[
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'active' => 'required',
            'building_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }

        $booking->update([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'active' => $request->input('active'),
            'building_id' => $request->input('building_id')
        ]);
        return response()->json( $booking,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Booking $booking
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Booking $booking)
    {
        Log::info('delete');
        if($booking->delete()){
            return response()->json([
                'message' => 'Booking delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }


    public function updateBookings(Request $request){
        Log::info('update');
        $bookingIds=explode(",",$request->get('bookingIds'));
        if($request->get('update') == 'Delete') {
            $booking = Booking::whereIn('id', $bookingIds)->delete();
        }
        elseif($request->get('update')=='Rejected'){
            $booking=Booking::whereIn('id', $bookingIds)->update(['status' => $request->get('update'),'reason'=>$request->get('reason')]);
        }
        elseif($request->get('update'))
            $booking=Booking::whereIn('id', $bookingIds)->update(['status' => $request->get('update')]);

        return response()->json( $booking,200);
    }
}
