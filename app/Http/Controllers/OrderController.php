<?php

namespace App\Http\Controllers;

use App\Model\Order;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        Log::info('indexxx');
        $query=Order::query();
        if ($request->has('search')) {
            $query->with('product')->with('tenant');
            $query->whereHas('product', function ($query) use ($request) {
                $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                    ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%');
            })
            ->orWhereHas('tenant', function ($query) use ($request) {
                $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                    ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%');
            });
        }

        if ($request->has('filter')) {
             $query->where('status', $request->get('filter'));
        }
        if ($request->has('sort')){
            $orders = $query->with(['product','tenant'])->orderBy($request->get('sort') ,$request->get('direction') )->paginate(5);
        }
        else
            $orders = $query->with(['product','tenant'])->orderBy('id','ASC')->paginate(5);

        return response()->json( $orders,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Log::info('store');

        $request->validate([
            'tenant_id' => 'required',
            'product_id' => 'required',
            'quantity' => 'required',
        ]);

        $product=Product::find($request->input('product_id'));
        $quantity= $request->input('quantity');
        $totalAmount= $product->price*$quantity;
        $order = Order::create([
            'tenant_id' => $request->input('tenant_id'),
            'product_id' => $request->input('product_id'),
            'quantity' => $request->input('quantity'),
            'total_amount' => $totalAmount,
            'status' => "New",
        ]);


        return response()->json( $order,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Order $order, Request $request)
    {
        Log::info('update.........');
        $validator = Validator::make($request->all(),[
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'active' => 'required',
            'building_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }

        $order->update([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'active' => $request->input('active'),
            'building_id' => $request->input('building_id')
        ]);

        return response()->json( $order,200);
    }

    public function updateOrders(Request $request){
        Log::info('update');
        $orderIds=explode(",",$request->get('orderIds'));
        if($request->get('update') == 'Delete') {
            $order = Order::whereIn('id', $orderIds)->delete();
        }
        elseif($request->get('update')=='Rejected'){
            $order=Order::whereIn('id', $orderIds)->update(['status' => $request->get('update'),'reason'=>$request->get('reason')]);
        }
        elseif($request->get('update'))
            $order=Order::whereIn('id', $orderIds)->update(['status' => $request->get('update')]);

        return response()->json( $order,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Order $order
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Order $order)
    {
        Log::info('delete');
        if($order->delete()){
            return response()->json([
                'message' => 'Order delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
