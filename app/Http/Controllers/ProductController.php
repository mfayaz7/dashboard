<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Http\Resources\ServiceResource;
use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $query=Product::query();
        if ($request->has('search')) {
            $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%')
                ->orWhere('price', 'like', '%' . $request->get('search') . '%');
        }

        if ($request->has('sort')){
            $products = $query->with('category')->orderBy($request->get('sort') ,$request->get('direction') )->paginate(5);
        }
        else
            $products = $query->with('category')->orderBy('id','ASC')->paginate(5);

        return response()->json( $products,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'price' => 'required',
            'category_id' => 'required',
        ]);
        $category=Category::find($request->input('category_id'));
        $product = Product::create([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'price' => $request->input('price'),
            'category_id' => $category->id
        ]);
        return new ProductResource($product);

//        return response()->json( $product,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Product $product, Request $request)
    {
        Log::info('update.........');
        $validator = Validator::make($request->all(),[
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'price' => 'required',
            'category_id' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }
        $category=Category::find($request->input('category_id'));
        $product->update([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'price' => $request->input('price'),
            'category_id' => $category->id
        ]);
        return new ProductResource($product);
       // return response()->json( $product,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        Log::info('delete');
        if($product->delete()){
            return response()->json([
                'message' => 'Product delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
