<?php

namespace App\Http\Controllers;

use App\Http\Resources\TenantResource;
use App\Model\Building;
use App\Model\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        Log::info('indexxx');
        $query=Tenant::query();
        if ($request->has('search')) {
            $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%');
        }

        if ($request->has('filter')) {
            $query->whereHas('building', function ($query) use ($request) {
                return $query->where('name', $request->get('filter'));
            });
        }
        $tenant = $query->with('building')->orderBy('id','ASC')->paginate(5);
        return response()->json( $tenant,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        Log::info('store');

        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'active' => 'required',
            'building_id' => 'required',
        ]);
        $building=Building::find($request->input('building_id'));
        $tenant = Tenant::create([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'active' => $request->input('active'),
            'building_id' => $building->id,
        ]);

        return new TenantResource($tenant);

//        return response()->json( $tenant,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Tenant  $tenant
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Tenant $tenant, Request $request)
    {
        Log::info('update.........');
        $request->validate([
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'building_id' => 'required',
        ]);

        $building=Building::find($request->input('building_id'));

            $tenant->name_en = $request->name_en;
            $tenant->name_ar = $request->name_ar;
            $tenant->building_id = $building->id;
            $tenant->save();

        return new TenantResource($tenant);

//        return response()->json( $tenant,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Tenant $tenant
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Tenant $tenant)
    {
        Log::info('delete');
        if($tenant->delete()){
            return response()->json([
                'message' => 'Tenant delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
    public function activeTenant(Request $request){
        Log::info('acive');
        $tenantIds=explode(",",$request->get('tenantIds'));
        if($request->get('update') == 'Active') {
            $tenant=Tenant::whereIn('id', $tenantIds)->update(['active' => '1']);
        }
        else if($request->get('update') == 'Deactive') {
            $tenant = Tenant::whereIn('id', $tenantIds)->update(['active' => '0']);
        }
        return response()->json( $tenant,200);
    }

}
