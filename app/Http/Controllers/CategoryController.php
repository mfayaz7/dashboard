<?php

namespace App\Http\Controllers;

use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        Log::info('categoriess');
        $query=Category::query();
        if ($request->has('search')) {
            $query->where('name_en', 'like', '%' . $request->get('search') . '%')
                ->orWhere('name_ar', 'like', '%' . $request->get('search') . '%')
                ->orWhere('desc_en', 'like', '%' . $request->get('search') . '%')
                ->orWhere('desc_ar', 'like', '%' . $request->get('search') . '%');
        }
        if ($request->has('sort')){
            $categories = $query->orderBy($request->get('sort') ,$request->get('direction') )->paginate(5);
        }
        else
            $categories = $query->orderBy('id','ASC')->paginate(5);

        return response()->json( $categories,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'desc_en' => 'required|min:1|max:255',
            'desc_ar' => 'required|min:1|max:255',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }

        $category = Category::create([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'desc_en' => $request->input('desc_en'),
            'desc_ar' => $request->input('desc_ar'),
        ]);
        return response()->json( $category,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Category $category, Request $request)
    {
        Log::info('update.........');
        $validator = Validator::make($request->all(),[
            'name_en' => 'required|min:3|max:255',
            'name_ar' => 'required|min:1|max:255',
            'desc_en' => 'required',
            'desc_ar' => 'required',
        ]);

        if($validator->fails()){
            return response()->json([
                'errors' => $validator->errors()->first(),
                'status_code' => 422
            ],422);
        }

        $category->update([
            'name_en' => $request->input('name_en'),
            'name_ar' => $request->input('name_ar'),
            'desc_en' => $request->input('desc_en'),
            'desc_ar' => $request->input('desc_ar')
        ]);

        return response()->json( $category,200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Category $category)
    {
        Log::info('delete');
        if($category->delete()){
            return response()->json([
                'message' => 'Category delete successfully',
                'status_code' => 200
            ],200);
        }else{
            return response()->json([
                'message' => 'Some error occurred,Please try again',
                'status_code' => 500
            ],500);
        }
    }
}
