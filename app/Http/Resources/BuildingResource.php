<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BuildingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>  $this->id,
            'property' =>  $this->property,
            'property_id' =>  $this->property->id,
            'created_at' => (string) $this->created_at,
            'deleted_at' => (string) $this->deleted_at,
            'name' => $this->name,
            'city' => $this->city,
            'address' => $this->address,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
