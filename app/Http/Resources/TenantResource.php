<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TenantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>  $this->id,
            'building' =>  $this->building,
            'building_id' =>  $this->building->id,
            'created_at' => (string) $this->created_at,
            'deleted_at' => (string) $this->deleted_at,
            'name_ar' => $this->name_ar,
            'name_en' => $this->name_en,
            'active' => $this->active,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
