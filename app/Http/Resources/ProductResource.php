<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>  $this->id,
            'category' =>  $this->category,
            'category_id' =>  $this->category->id,
            'created_at' => (string) $this->created_at,
            'deleted_at' => (string) $this->deleted_at,
            'name_ar' => $this->name_ar,
            'name_en' => $this->name_en,
            'price' => $this->price,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
